/**
 * Mixpanel authentication route
 *
 * @author: Chris Moyer <cmoyer@aci.info>
 */
'use strict';

var _ = require('lodash');
var crypto = require('crypto');
var request = require('request');

var express = require('express');
var router = express.Router();
module.exports = router;

var BASE_URL = 'http://mixpanel.com/api/2.0/';

/**
 * Redirect requests to Mixpanel
 *
 */
router.get('/:action', function(req, res, next){

	// Add our required parameters
	var query = _.clone(req.query);
	query.api_key = req.username;
	query.expire = Math.round((new Date()).getTime()/1000) + 600;

	// Create the MD5 Signature
	var param_names = _.keys(query).sort();
	var sigProps = [];
	_.forEach(param_names, function(param){
		sigProps.push(param + '=' + query[param]);
	});
	var md5 = crypto.createHash('md5');
	md5.update(sigProps.join('') + req.password);
	query.sig = md5.digest('hex');
	res.set('Content-Type', 'application/json; charset=utf-8');
	request.get({
		url: BASE_URL + req.params.action,
		qs: query,
	}).pipe(res);
});
