Klipfolio REST Adapter
======================

Klipfolio (http://klipfolio.com) only supports a few basic HTTP authentication mechanisms. This simple web-adapter allows you to connect to services that use other authentication mechanisms, such as the MixPanel API.


Currently only the MixPanel API is supported.

Future additions will include AWS CloudWatch.


Mixpanel API
------------

The mixpanel API can be connected to by using your API Key and API Secret Key. Simply pass in the API Key as a username, and the API Secret Key as the password using basic HTTP authentication. Start up this server and then hit /mixpanel/<action>?url-parameters

Essentially, this just re-directs directly to the mixpanel API, so a query to:

	http://your-server.com/mixpanel/events?interval=7&type=unique&unit=day&event=%5B%22Launched%22%5D

Would translate into:

	http://mixpanel.com/api/2.0/events?interval=7&type=unique&unit=day&event=%5B%22Launched%22%5D

And return the results authenticated as MixPanel requires.


Adding New Routes
=================

Just make a pull request, and add a new file under routes/, following how it's done in routes/mixpanel.js.
