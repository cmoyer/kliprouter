/**
 * Klipfolio Authentication Middleware and Router.
 * Adds support for AWS and Mixpanel authentication mechanisms.
 *
 *
 * @author: Chris Moyer <cmoyer@aci.info>
 */
'use strict';

var express = require('express');
var app = express();
var fs = require('fs');


// Simple status route requires no authentication
app.get('/status', function(req, res, next){
	res.json({
		status: 'OK',
		code: 200,
	});
});

// Translate basic authentication
app.use(function(req, res, next){
	// Decode the basic authentication
	var header=req.headers.authorization || '';
	var token=header.split(/\s+/).pop() || '';
	var auth=new Buffer(token, 'base64').toString();
	var parts=auth.split(/:/);

	req.username=parts[0];
	req.password=parts[1];

	if(!req.username || !req.password){
		res.status(401).send('Authentication required');
	} else {
		next();
	}
});

// Load each route
fs.readdirSync('./routes').forEach(function(fname){
	if(/^.*\.js$/.test(fname)){
		var route = require('./routes/' + fname);
		app.use('/' + fname.split('.')[0], route);
	}
});

// Listen
app.listen(process.env.PORT || 8000);
